<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="init.jsp"%>
</head>
<body>
	<%@ include file="header.html"%>

	<div class="table-row">
		<div class="left-side">
			<a href="${pieceLink}"><img class="center-piece"
				src="${imageLink}" alt="Random piece" /></a>
		</div>
		<div class="right-side">
			<p class="body-text">
				contact us<br /> <br /> <br /> address<br /> steph murray johnson
				jewelry<br /> po box xxxx<br /> Chicago Il 606xx<br /> <br /> phone<br />
				773.573.3773<br /> <br /> email<br />
				info@stephmurrayjohnsonjewelry.com
			</p>
		</div>
	</div>


	<%@ include file="footer-links.html"%>
</body>
</html>
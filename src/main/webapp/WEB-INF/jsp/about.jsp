<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="init.jsp" %>
</head>
<body>
	<h1>about!</h1>
	
	<!--  Out image -->
	<!--  ${image} -->
	<img src="${imageLink}" alt="The pic" />
	
	<%@ include file="footer-links.html"%>
</body>
</html>
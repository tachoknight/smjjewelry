<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="init.jsp"%>
</head>
<body>
	<%@ include file="header.html"%>

	<table>
		<tr>
			<td>${imageTable}</td>
			<td><table>
					<tr>
						<td>${pieceSKU}</td>
						<td>${pieceDescription}</td>
						<td>${piecePrice}</td>
					</tr>
				</table></td>
		</tr>
	</table>
	<%@ include file="footer-links.html"%>
</body>
</html>
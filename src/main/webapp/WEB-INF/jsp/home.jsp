
<%@ page session="false"%>
<html>
<head>
<%@ include file="init.jsp"%>
<link rel="stylesheet" type="text/css" href="resources/threecol.css">
</head>
<body>
	<%@ include file="header.html"%>

	<div class="colmask threecol">
		<div class="colmid">
			<div class="colleft">
				<div class="col1">
					<a href="${pieceLink1}"><img class="center-piece"
						src="${imageLink1}" alt="Random piece 1" /></a>
				</div>
				<div class="col2">
					<a href="${pieceLink2}"><img class="center-piece"
						src="${imageLink2}" alt="Random piece 2" /></a>
				</div>
				<div class="col3">
					<a href="${pieceLink3}"><img class="center-piece"
						src="${imageLink3}" alt="Random piece 3" /></a>
				</div>
			</div>
		</div>
	</div>

	<%@ include file="footer-links.html"%>
</body>
</html>

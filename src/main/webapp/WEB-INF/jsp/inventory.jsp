<%@taglib
	uri="http://www.springframework.org/tags"
	prefix="spring"%>
<%@taglib
	uri="http://www.springframework.org/tags/form"
	prefix="form"%>
<%@taglib
	uri="http://java.sun.com/jsp/jstl/core"
	prefix="c"%>
<%@ page session="false"%>
<html>
<head>
<meta charset="utf-8" />
<title>Stephanie Murray Johnson Jewelry Inventory Control</title>
<link
	rel="stylesheet"
	href="http://code.jquery.com/ui/1.10.1/themes/base/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.9.1.js">
	
</script>
<script src="http://code.jquery.com/ui/1.10.1/jquery-ui.js"></script>
<link
	rel="stylesheet"
	href="/resources/demos/style.css" />

<link
	href="http://twitter.github.com/bootstrap/assets/css/bootstrap.css"
	rel="stylesheet">
<link
	href="http://twitter.github.com/bootstrap/assets/css/bootstrap-responsive.css"
	rel="stylesheet">

<style>
body {
	font-size: 62.5%;
}

label,input {
	display: block;
}

input.text {
	margin-bottom: 12px;
	width: 95%;
	padding: .4em;
}

fieldset {
	padding: 0;
	border: 0;
	margin-top: 25px;
}

h1 {
	font-size: 1.2em;
	margin: .6em 0;
}

div#users-contain {
	width: 350px;
	margin: 20px 0;
}

div#users-contain table {
	margin: 1em 0;
	border-collapse: collapse;
	width: 100%;
}

div#users-contain table td,div#users-contain table th {
	border: 1px solid #eee;
	padding: .6em 10px;
	text-align: left;
}

.ui-dialog .ui-state-error {
	padding: .3em;
}

.validateTips {
	border: 1px solid transparent;
	padding: 0.3em;
}
</style>
<script>
	function editPiece(id, name, sku, price, description, notes) {
		$("#id").val(id);
		$("#name").val(name);
		$("#sku").val(sku);
		$("#price").val(price);
		$("#description").val(description);
		$("#notes").val(notes);

		$("#dialog-form").dialog("open");
	}

	$(function() {

		$("#dialog-form").dialog({
			autoOpen : false,
			height : 600,
			width : 650,
			modal : true,
			buttons : {
				"Add/Modify Piece" : function() {

					document.forms["newpiece"].submit();

					$(this).dialog("close");
				},
				Cancel : function() {
					$(this).dialog("close");
				}
			},
			close : function() {
			}
		});

		$("#create-piece").button().click(function() {
			$("#dialog-form").dialog("open");
		});
	});
</script>
</head>
<body>
	<div
		id="dialog-form"
		title="Add/Edit a Piece">
		<p class="validateTips">All fields are required.</p>
		<form:form
			name="newpiece"
			method="post"
			action="add"
			commandName="piece"
			class="form-vertical">
			<fieldset>
				<label for="id">ID</label>
				<input
					readonly
					type="text"
					name="id"
					id="id"
					class="text ui-widget-content ui-corner-all" />
				<label for="name">Name</label>
				<input
					type="text"
					name="name"
					id="name"
					class="text ui-widget-content ui-corner-all" />
				<label for="sku">SKU</label>
				<input
					type="text"
					name="sku"
					id="sku"
					value=""
					class="text ui-widget-content ui-corner-all" />
				<label for="price">Price</label>
				<input
					type="text"
					name="price"
					id="price"
					value=""
					class="text ui-widget-content ui-corner-all" />
				<label for="description">Description</label>
				<input
					type="text"
					name="description"
					id="description"
					value=""
					class="text ui-widget-content ui-corner-all" />
				<label for="notes">Notes</label>
				<input
					type="text"
					name="notes"
					id="notes"
					value=""
					class="text ui-widget-content ui-corner-all" />
			</fieldset>
		</form:form>
	</div>
	<div
		id="users-contain"
		class="ui-widget">
		<h1>Existing Pieces:</h1>
		<table
			id="users"
			class="ui-widget ui-widget-content">
			<thead>
				<tr class="ui-widget-header ">
					<th>ID</th>
					<th>Name</th>
					<th>SKU</th>
					<th>Price</th>
					<th>Description</th>
					<th>Notes</th>
					<th>Images</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach
					items="${pieces}"
					var="piece">
					<tr>
						<td>${piece.id}</td>
						<td>${piece.name}</td>
						<td>${piece.sku}</td>
						<td>${piece.price}</td>
						<td>${piece.description}</td>
						<td>${piece.notes}</td>
						<td>
							<!-- Images here -->
							<table>
								<c:forEach
									items="${piece.photos}"
									var="photo">
									<tr>
										<td>
											<a
												target="_blank"
												href="../image?id=${piece.id}&imgid=${photo.id}"> <img
												class="center-piece"
												width="100"
												height="100"
												src="../image?id=${piece.id}&imgid=${photo.id}"
												alt="Nice Piece" />
											</a>
										</td>
										<td>ID ${photo.id}</td>
										<td>
											<!-- Image delete button -->
											<form
												action="imgdel?id=${piece.id}&imgid=${photo.id}"
												method="post">
												<input
													type="submit"
													class="btn btn-danger btn-mini"
													value="Delete" />
											</form>
										</td>
									</tr>
								</c:forEach>
							</table>

							<form:form
								action="addimage"
								method="post"
								enctype="multipart/form-data"
								modelAttribute="imageUploadForm">
								<form:input
									path="pieceId"
									name="pieceId"
									type="hidden"
									value="${piece.id}" />
								<!-- TODO: XYZZY - HEY, HARD CODED 0!!!! -->
								<form:input
									name="files[0]"
									path="files"
									type="file" />
								<input
									type="submit"
									class="btn btn-warning btn-mini"
									value="Upload" />
							</form:form>

							<!-- End Image part -->
						</td>
						<td>
							<input
								type="submit"
								class="btn btn-warning btn-mini"
								value="Edit"
								onclick="editPiece('${piece.id}', '${piece.name}', '${piece.sku}', '${piece.price}', '${piece.description}', '${piece.notes}')" />
							<p />
							<form
								action="delete/${piece.id}"
								method="post">
								<input
									type="submit"
									class="btn btn-danger btn-mini"
									value="Delete" />
							</form>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
	<button id="create-piece">Add New Piece</button>
</body>
</html>

package com.smjjewelry.service;

import org.apache.commons.collections.ListUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.smjjewelry.model.Piece;
import com.smjjewelry.model.PieceImage;

import javax.persistence.EntityManager;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;

//@NamedQueries({ @NamedQuery(name = "findPieceBySKU", query = "SELECT p FROM Piece p WHERE p.sku = :sku"),
//				@NamedQuery(name = "getPieceCount", query = "SELECT count(p.id) FROM Piece p") })
@NamedQueries({ @NamedQuery(name = "findPieceBySKU", query = "SELECT p FROM Piece p WHERE p.sku = :sku") })
@Service
public class InventoryServiceImpl implements InventoryService
{
	private static final Logger	logger	= LoggerFactory.getLogger(InventoryServiceImpl.class);

	@PersistenceContext
	EntityManager				em;

	@SuppressWarnings("unchecked")
	@Transactional
	@CacheEvict(value = "piece_cache", key = "#piece.id")
	public void addPiece(	Piece piece,
							boolean doMerge)
	{
		if (doMerge)
		{
			/*
			 * If we're here, that means that we're editing, in which case the
			 * data of the piece was edited, sure, but we need to get the images
			 * from the existing object so we don't lose them
			 */
			Piece oldPiece = getPiece(piece.getId());
			List<PieceImage> oldSet = oldPiece.getPhotos();
			List<PieceImage> newSet = piece.getPhotos();
			/* Merge the two sets of photos */
			piece.setPhotos(ListUtils.union(oldSet, newSet));

			/* And save it to the database */
			em.merge(piece);
		}
		else
		{
			/* And save it to the database */
			em.persist(piece);
		}
	}

	@Transactional
	public List<Piece> listPieces()
	{
		System.out.println("in listPieces!");

		CriteriaQuery<Piece> c = em.getCriteriaBuilder().createQuery(Piece.class);
		c.from(Piece.class);
		return em.createQuery(c).getResultList();
	}

	@Transactional
	public void removePiece(Integer id)
	{
		Piece piece = em.find(Piece.class, id);
		if (null != piece)
		{
			em.remove(piece);
		}
	}

	@Transactional
	@Cacheable(value = "piece_cache")
	public Piece getPiece(Integer id)
	{
		System.out.println("In getPiece() with id of " + id);

		Piece piece = em.find(Piece.class, id);
		if (null != piece)
		{
			return piece;
		}

		return null;
	}

	@Transactional
	public byte[] getImage(	Integer pieceId,
							Integer imageId)
	{
		System.out.println("In getImage() with pieceID of " + pieceId + " and imageId of " + imageId);
		Piece piece = em.find(Piece.class, pieceId);
		if (null == piece)
		{
			return null;
		}

		return piece.getImage(imageId);
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<Piece> getRandomPieces(int count)
	{
		logger.debug("in getRandomPieces!");

		return em.createQuery("SELECT p FROM Piece p").setMaxResults(count).getResultList();
	}

	@Override
	@Transactional
	public Piece getBySku(String sku)
	{
		Piece piece = (Piece) em.createQuery("SELECT p FROM Piece p WHERE p.sku = :sku").setParameter("sku", sku).getSingleResult();

		if (null != piece)
		{
			return piece;
		}

		return null;
	}

	@Transactional
	public void removePieceImage(	Integer pieceId,
									Integer imageId)
	{
		Piece piece = getPiece(pieceId);
		List<PieceImage> photos = new ArrayList<PieceImage>(piece.getPhotos());
		int pos = 0;
		for (PieceImage pi : photos)
		{
			if (pi.getId().equals(imageId))
			{
				logger.info("Found the piece to remove...");

				/* Remove the image from the list ... */
				photos.remove(pos);

				/*
				 * Clear out the photos (which fixes a problem with orphans in
				 * Hibernate)
				 */
				piece.getPhotos().clear();

				/* And put our new piece list in its place */
				piece.getPhotos().addAll(photos);

				/*
				 * And save it back ... note that because we are *replacing* the
				 * existing piece with the images, we do not want to merge, we
				 * want to persist it. Probably should be a good idea to clean
				 * all this up at some point
				 */
				addPiece(piece, false);

				break;
			}

			++pos;
		}
	}

	private Long getPieceCount()
	{
		return (Long) em.createQuery("select count(p.id) from Piece p").getSingleResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public Piece getRandomPiece()
	{
		int rowCount = getPieceCount().intValue();
		logger.debug("Row count is " + rowCount);
		List<Piece> pieceList = em.createQuery("SELECT p FROM Piece p")
									.setMaxResults(rowCount)
									.getResultList();

		return pieceList.get(new Random().nextInt(pieceList.size()));
	}
}

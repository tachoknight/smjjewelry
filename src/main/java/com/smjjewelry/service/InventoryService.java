package com.smjjewelry.service;

import java.util.List;

import com.smjjewelry.model.Piece;

public interface InventoryService
{
	/*
	 * Related to inventory
	 */
	public void addPiece(Piece piece, boolean doMerge);

	public List<Piece> listPieces();

	public void removePiece(Integer id);
	
	public void removePieceImage(Integer pieceId, Integer imageId);
	
	public Piece getPiece(Integer id);
	
	public byte [] getImage(Integer pieceId, Integer imageId);
	
	/*
	 * Catalog stuff
	 */
	public List<Piece> getRandomPieces(int count);
	public Piece getRandomPiece();
	
	public Piece getBySku(String sku);
}

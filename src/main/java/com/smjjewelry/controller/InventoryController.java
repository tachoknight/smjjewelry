package com.smjjewelry.controller;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import com.smjjewelry.model.ImageUploadForm;
import com.smjjewelry.model.Piece;
import com.smjjewelry.model.PieceImage;
import com.smjjewelry.service.InventoryService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class InventoryController
{
	private static final Logger	logger	= LoggerFactory.getLogger(InventoryController.class);

	@Autowired
	private InventoryService	inventoryService;

	@RequestMapping("/inventory/")
	public String listPieces(Map<String, Object> map)
	{
		map.put("piece", new Piece());
		map.put("imageUploadForm", new ImageUploadForm());
		map.put("pieces", inventoryService.listPieces());

		return "inventory";
	}

	@RequestMapping(value = "/inventory/add", method = RequestMethod.POST)
	public String addPiece(	@ModelAttribute("piece") Piece piece,
							BindingResult result)
	{
		if (piece.getId() != null && piece.getId() > 0)
			inventoryService.addPiece(piece, true);
		else
			inventoryService.addPiece(piece, false);

		return "redirect:/inventory/";
	}

	@RequestMapping("/inventory/delete/{pieceId}")
	public String deletePiece(@PathVariable("pieceId") Integer pieceId)
	{
		logger.debug("About to delete piece with id of " + pieceId);

		inventoryService.removePiece(pieceId);

		return "redirect:/inventory/";
	}

	@RequestMapping("/inventory/edit/{pieceId}")
	public String editPiece(@PathVariable("pieceId") Integer pieceId,
							ModelMap model)
	{
		logger.debug("About to edit piece with id of " + pieceId);

		/*
		 * What we're going to do is get the piece from the database and get the
		 * fields back into the form for it.
		 */
		Piece piece = inventoryService.getPiece(pieceId);

		model.addAttribute("piece", piece);

		return "inventory";
	}

	/*
	 * For adding images
	 */
	@RequestMapping(value = "/inventory/addimage", method = RequestMethod.POST)
	public String addPieceImage(@ModelAttribute("imageUploadForm") ImageUploadForm imageUploadForm,
								Model map)
	{
		logger.info("In addPieceImage with id of " + imageUploadForm.getPieceId());

		List<MultipartFile> files = imageUploadForm.getFiles();

		logger.info("File size is " + (files == null ? 0 : files.size()));

		List<String> fileNames = new ArrayList<String>();

		if (null != files && files.size() > 0)
		{
			for (MultipartFile multipartFile : files)
			{
				try
				{
					String fileName = multipartFile.getOriginalFilename();
					fileNames.add(fileName);
					logger.info("Adding file " + fileName);

					/* Now start putting the image info together */
					PieceImage pi = new PieceImage();
					/* TODO: IMAGE SEQUENCE */
					// pi.setSequence(1); /* Temporarily hard-coded */
					pi.setFileName(fileName);

					/*
					 * Now we handle file content -
					 * multipartFile.getInputStream()
					 */
					pi.setImage(IOUtils.toByteArray(multipartFile.getInputStream()));

					/* And get the piece for this image */
					Piece p = inventoryService.getPiece(imageUploadForm.getPieceId());
					pi.setPiece(p);
					List<PieceImage> images = new ArrayList<PieceImage>();
					images.add(pi);

					p.setPhotos(images);
					inventoryService.addPiece(p, true);
				}
				catch (IOException e)
				{
					logger.error("Didn't add the image with filename of " + multipartFile.getOriginalFilename()
									+ " because of "
									+ e.getLocalizedMessage(), e);
				}
			}
		}

		return "redirect:/inventory/";
	}

	@RequestMapping(value = "/inventory/imgdel", method = RequestMethod.POST)
	public String removeImage(HttpServletRequest request,
							HttpServletResponse response)
	{
		int pieceID = Integer.valueOf(request.getParameter("id"));
		int imageID = Integer.valueOf(request.getParameter("imgid"));

		logger.info("In removeImage with id of " + pieceID + " and image id of " + imageID);

		inventoryService.removePieceImage(pieceID, imageID);

		return "redirect:/inventory/";
	}
}

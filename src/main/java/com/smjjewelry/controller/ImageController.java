package com.smjjewelry.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.smjjewelry.service.InventoryService;

@Controller
public class ImageController
{
	private static final Logger	logger	= LoggerFactory.getLogger(ImageController.class);

	@Autowired
	private InventoryService	inventoryService;

	@RequestMapping(value = "/image", method = RequestMethod.GET)
	public void getImage(	HttpServletRequest request,
							HttpServletResponse response)
	{
		int pieceID = Integer.valueOf(request.getParameter("id"));		
		int imageID = Integer.valueOf(request.getParameter("imgid"));
		
		logger.info("In getImage with id of " + pieceID + " and image id of " + imageID);

		try
		{
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			baos.write(inventoryService.getImage(pieceID, imageID));

			response.setContentType("image/jpg");
			response.setHeader("Content-disposition", "inline; filename=smjpiece.jpg");
			response.setContentLength(baos.size());

			ServletOutputStream sos = response.getOutputStream();
			baos.writeTo(sos);
			sos.flush();
		}
		catch (NullPointerException e)
		{
			logger.warn("Hmm...got an null pointer exception (piece probably doesn't exist yet): " + e.getLocalizedMessage(),
						e);
		}
		catch (IOException e)
		{
			logger.error("Hmm...got an ioexception: " + e.getLocalizedMessage(), e);
		}
	}
}
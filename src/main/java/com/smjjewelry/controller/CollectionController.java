package com.smjjewelry.controller;

import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.smjjewelry.model.Piece;
import com.smjjewelry.model.PieceImage;
import com.smjjewelry.service.InventoryService;

@Controller
public class CollectionController
{
	private static final Logger	logger	= LoggerFactory.getLogger(CollectionController.class);

	@Autowired
	private InventoryService	inventoryService;

	/**
	 * This method is to get the details of a particular piece by sku number
	 * 
	 * @param sku
	 * @param model
	 * @return the collection jsp page
	 */
	@RequestMapping(value = "/collection", method = RequestMethod.GET)
	public String getDetails(	@RequestParam("sku") String sku,
								Model model)
	{
		logger.info("Called the collection page for piece id " + sku);

		/* Get the piece details */
		Piece myPiece = inventoryService.getBySku(sku);
		System.out.println("-->" + myPiece.getDescription());

		List<PieceImage> images = myPiece.getPhotos();

		String imageTable = "<table border=\"5\">";
		for (PieceImage pi : images)
		{
			/* TODO: IMAGE SEQUENCE */
			imageTable += "<tr><td><img src=\"image?id=" + myPiece.getId() + "&imgid=" + pi.getId() + "\"</td></tr></table>";
		}

		imageTable += "</table>";

		model.addAttribute("imageTable", imageTable);
		model.addAttribute("pieceDescription", myPiece.getDescription());
		model.addAttribute("piecePrice", myPiece.getPrice());
		model.addAttribute("pieceSKU", myPiece.getSku());

		return "collection";
	}
}

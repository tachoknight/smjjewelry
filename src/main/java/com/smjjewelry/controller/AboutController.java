package com.smjjewelry.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Locale;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.smjjewelry.model.Piece;
import com.smjjewelry.service.InventoryService;

@Controller
public class AboutController
{
	private static final Logger	logger	= LoggerFactory.getLogger(AboutController.class);

	@Autowired
	private InventoryService	inventoryService;

	@RequestMapping(value = "/about", method = RequestMethod.GET)
	public String about(Locale locale,
						Model model)
	{
		logger.debug("Called the about page");

		Piece myPiece = inventoryService.getRandomPiece();
		if (myPiece != null)
		{
			logger.debug("-->" + myPiece.getDescription());

			model.addAttribute("imageLink", "image?id=" + myPiece.getId() + "&imgid="+ myPiece.getRandomImageId());
		}
		else
		{
			logger.warn("Got a null object for random piece!");
		}

		return "about";
	}
}

package com.smjjewelry.controller;

import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.smjjewelry.model.Piece;
import com.smjjewelry.service.InventoryService;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController
{
	private static final Logger	logger	= LoggerFactory.getLogger(HomeController.class);

	@Autowired
	private InventoryService	inventoryService;

	/**
	 * Simply selects the home view to render by returning its name. We have an
	 * array of values, will response to "", "/" and "/home" (used for clicking
	 * on the main banner image)
	 */
	@RequestMapping(value = { "/", "/home", " * " }, method = RequestMethod.GET)
	public String home(	Locale locale,
						Model model)
	{
		/* And here we go ... */
		logger.info("Got a new request for the main page...the client locale is " + locale.toString());

		List<Piece> randomPieces = inventoryService.getRandomPieces(3);

		int pos = 0;
		for (Piece piece : randomPieces)
		{
			model.addAttribute("imageLink" + (pos + 1), "image?id=" + piece.getId() + "&imgid=" + piece.getRandomImageId());
			model.addAttribute("pieceLink" + (pos + 1), "collection?sku=" + piece.getSku());
			++pos;
		}

		return "home";
	}

}

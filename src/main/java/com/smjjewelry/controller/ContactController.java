package com.smjjewelry.controller;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.smjjewelry.model.Piece;
import com.smjjewelry.service.InventoryService;

@Controller
public class ContactController
{
	private static final Logger logger = LoggerFactory.getLogger(ContactController.class);
	
	@Autowired
	private InventoryService	inventoryService;

	@RequestMapping(value = "/contact", method = RequestMethod.GET)
	public String about(Locale locale, Model model)
	{
		logger.debug("Called the contact page");


		List<Piece> randomPieces = inventoryService.getRandomPieces(1);
		
		model.addAttribute("imageLink", "image?id=" + randomPieces.get(0).getId());
		model.addAttribute("pieceLink", "collection?sku=" + randomPieces.get(0).getSku());

		return "contact";
	}
}

package com.smjjewelry.model;

import java.io.Serializable;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

/* The class for uploading an image to the database */
public class ImageUploadForm implements Serializable
{

	private static final long	serialVersionUID	= 2467997848299295770L;

	private Integer				pieceId;
	private List<MultipartFile>	files;
	
	public Integer getPieceId()
	{
		return pieceId;
	}

	public void setPieceId(Integer pieceId)
	{
		this.pieceId = pieceId;
	}

	public List<MultipartFile> getFiles()
	{
		return files;
	}

	public void setFiles(List<MultipartFile> files)
	{
		this.files = files;
	}
}

package com.smjjewelry.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;

@Entity
public class Piece
{
	private static final Logger	logger	= LoggerFactory.getLogger(Piece.class);

	@Id
	@GeneratedValue
	private Integer				id;
	private String				name;
	private String				sku;
	private String				price;
	private String				description;
	private String				notes;

	@OneToMany(	fetch = FetchType.EAGER,
				targetEntity = PieceImage.class,
				mappedBy = "piece",
				cascade = { CascadeType.ALL },
				orphanRemoval = true)
	// @OrderBy("lastname ASC")
	List<PieceImage>			photos	= new ArrayList<PieceImage>();

	public List<PieceImage> getPhotos()
	{
		return photos;
	}

	public void setPhotos(List<PieceImage> photos)
	{
		this.photos = photos;
	}

	public Integer getRandomImageId()
	{
		logger.debug("Going to get a random inage from this piece (size is " + photos.size());

		return photos.get(new Random().nextInt(photos.size())).getId();
	}

	// @Cacheable("piece_image_cache")
	public byte[] getImage(Integer imageID)
	{
		logger.debug("**** IMAGE " + imageID);
		logger.debug("There are " + photos.size() + " photos in the collection for this piece");

		for (PieceImage pi : photos)
		{
			logger.debug("####### WITH " + pi.getFileName() + " " + pi.getId());
			if (pi.getId().equals(imageID))
				return pi.getImage();
		}

		return null;
	}

	public Piece()
	{

	}

	public String getNotes()
	{
		return notes;
	}

	public void setNotes(String notes)
	{
		this.notes = notes;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public String getSku()
	{
		return sku;
	}

	public void setSku(String sku)
	{
		this.sku = sku;
	}

	public String getPrice()
	{
		return price;
	}

	public void setPrice(String price)
	{
		this.price = price;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}
}

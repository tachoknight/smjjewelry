package com.smjjewelry.model;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.SQLException;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

import org.hibernate.Hibernate;

@Entity
public class PieceImage
{
	@Id
	@GeneratedValue
	private Integer	id;
	//private Integer	pieceID;
	private Integer	sequence;
	private String	fileName;
	@Lob
	private byte[]	image;

	@ManyToOne(targetEntity = Piece.class)
	@JoinColumn(name = "piece_id", referencedColumnName="id")
	Piece			piece;

	
	public Piece getPiece()
	{
		return piece;
	}

	public void setPiece(Piece piece)
	{
		this.piece = piece;
	}

	public PieceImage()
	{

	}

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public Integer getSequence()
	{
		return sequence;
	}

	public void setSequence(Integer sequence)
	{
		this.sequence = sequence;
	}

	public String getFileName()
	{
		return fileName;
	}

	public void setFileName(String fileName)
	{
		this.fileName = fileName;
	}

	public byte[] getImage()
	{
		return image;
	}

	public void setImage(byte[] image)
	{
		this.image = image;
	}

	@SuppressWarnings("unused")
	private void setBlob(Blob imageBlob)
	{
		this.image = toByteArray(imageBlob);
	}

	@SuppressWarnings("unused")
	private Blob getBlob()
	{
		return Hibernate.createBlob(this.image);
	}

	private byte[] toByteArray(Blob fromImageBlob)
	{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try
		{
			return toByteArrayImpl(fromImageBlob, baos);
		}
		catch (Exception e)
		{
		}
		return null;
	}

	private byte[]
			toByteArrayImpl(Blob fromImageBlob, ByteArrayOutputStream baos)	throws SQLException,
																			IOException
	{
		byte buf[] = new byte[4000];
		int dataSize;
		InputStream is = fromImageBlob.getBinaryStream();

		try
		{
			while ((dataSize = is.read(buf)) != -1)
			{
				baos.write(buf, 0, dataSize);
			}
		}
		finally
		{
			if (is != null)
			{
				is.close();
			}
		}
		return baos.toByteArray();
	}

}
